<?php

namespace Patterns\Creational\FactoryMethod;


abstract class Developer
{

    abstract public function factoryMethod(): Product;


    public function createProduct(): string
    {
        $product = $this->factoryMethod();
        $result = "Developer: everything is working" .
            $product->order();

        return $result;
    }
}

class GameDeveloper extends Developer
{
    public function factoryMethod(): Product
    {
        return new Games();
    }
}

class WebDeveloper extends Developer
{
    public function factoryMethod(): Product
    {
        return new Website();
    }
}

interface Product
{
    public function order(): string;
}

class Games implements Product
{
    public function order(): string
    {
        return "{Result of the Games}";
    }
}

class Website implements Product
{
    public function order(): string
    {
        return "{Result of the Website}";
    }
}

function clientCode(Developer $developer)
{
    // ...
    echo "Client: The developer is still working on my project. \n"
        . $developer->createProduct();
    // ...
}

echo "This is the result of GameDeveloper. \n";
clientCode(new GameDeveloper());
echo "\n\n";

echo "This is the result of WebDeveloper. \n";
clientCode(new WebDeveloper());