<?php

namespace Patterns;

use Exception;

//№1
abstract class Publication
{
    abstract public function do_print();
}

class News extends Publication
{
    public function do_print()
    {
        echo '<h4<Новость</h4<';
    }
}
class Announcement extends Publication
{
    public function do_print()
    {
        echo '<h4<Объявление</h4<';
    }
}
class Article extends Publication
{
    public function do_print()
    {
        echo '<h4<Статья</h4<';
    }
}

$publications[] = new News();
$publications[] = new Announcement();
$publications[] = new Article();

foreach ($publications as $publication)
{
    if ($publication instanceof Publication) {
        $publication->do_print();
    }
    else {
        throw new Exception('Error');
    }
}

//№2
interface Recipe
{
    public function methodology();
}


class Cake implements Recipe
{
    public function methodology()
    {

    }
}

//№3

class Animal
{
    public $name;

    public function Greet()
    {
        return "Hello, I'm some sort of animal and my name is " . $this->name;
    }
}

class Dog extends Animal
{
    public function Greet()
    {
        return "Hello, I'm a dog and my name is " . $this->name;
    }
}

//№4

interface First
{
    public function foo(int $x);
}

interface Second
{
    public function bar(string $s);
}

interface Third
    extends First, Second
{
    public function baz(array $a);
}

assert(true === method_exists(Third::class, 'foo'));
assert(true === method_exists(Third::class, 'bar'));
assert(true === method_exists(Third::class, 'baz'));

interface IntSumInterface
{
    public function sum(int $x, int $y): int;
}

interface IntMultInterface
{
    public function mult(int $x, int $y): int;
}

class Math
    implements IntSumInterface, IntMultInterface
{
    public function sum(int $x, int $y): int
    {
        return $x + $y;
    }

    public function mult(int $x, int $y): int
    {
        return $x * $y;
    }
}