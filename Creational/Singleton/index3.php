<?php

namespace Patterns\Creational\Singleton;

class Singleton
{
    public $date;

    private static $instance = null;

    public static function getInstance()
    {
        if (empty(self::$instance))
            self::$instance = new self();

        return self::$instance;
    }

    private function __construct()
    {
        echo "<h1>Время</h1>";
        date_default_timezone_set("Europe/Kiev");
        $this->date = date("Y-m-d H:i:s");
    }

    private function __clone()
    {

    }

}

$test1 = Singleton::getInstance();
echo $test1->date;

echo "<br>";

$test2 = Singleton::getInstance();
echo $test2->date;

