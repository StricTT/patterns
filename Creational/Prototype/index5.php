<?php

namespace Patterns\Creational\Prototype;


class Book
{
    public $title;

    public $category;

    public function __construct($title, $category)
    {
        $this->title = $title;
        $this->category = $category;
    }

    public function __clone()
    {

    }

}

class FoodBook extends Book
{
    public function __clone()
    {

    }

}

class DrinkBook extends Book
{
    public function __clone()
    {

    }
}

$foodbook = new FoodBook('What to eat', 'Food');
$drinkbook = new DrinkBook('Speedy Cocktails', 'Drink');

$book1 = clone $foodbook;
echo ('Book 1 title: '.$book1->title);
echo '<br>';
echo ('Book 1 category: '.$book1->category);
echo '<br>';
echo('Price: '. rand(100,1000));

echo '<br>';
echo '<br>';

$book2 = clone $drinkbook;
echo ('Book 2 title: '.$book2->title);
echo '<br>';
echo ('Book 2 category: '.$book2->category);
echo '<br>';
echo('Price: '. rand(100,1000));
