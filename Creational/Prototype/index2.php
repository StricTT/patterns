<?php

namespace Patterns\Creational\Prototype;


class Book
{
    public $title;

    public $category;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }


    public function __clone()
    {

    }
}

class FoodBook extends Book
{
    public function __construct()
    {
        $this->category = 'FoodBook';
    }

    public function __clone()
    {

    }

}

class DrinkBook extends Book
{
    public function __construct()
    {
        $this->category = 'DrinkBook';
    }

    public function __clone()
    {

    }
}

$foodbook = new FoodBook();
$drinkbook = new DrinkBook();

$book1 = clone $foodbook;
$book1->setTitle('What to eat');
echo('Book 1 category: '.$book1->getCategory());
echo '<br>';
echo('Book 1 title: '.$book1->getTitle());
echo '<br>';
echo('Price: '. rand(100,1000));

echo '<br>';
echo '<br>';

$book2 = clone $drinkbook;
$book2->setTitle('Speedy Cocktails');
echo('Book 2 category: '.$book2->getCategory());
echo '<br>';
echo('Book 2 title: '.$book2->getTitle());
echo '<br>';
echo('Price: '. rand(100,1000));

