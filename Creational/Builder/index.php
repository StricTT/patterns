<?php

namespace Patterns\Creational\Builder;


interface Builder
{
    public function design(): void;

    public function drawing(): void;

    public function engineering(): void;
}


class SlidingWardrobeBuilder implements Builder
{
    private $wardrobe;

    public function __construct()
    {
        $this->reset();
    }

    public function reset(): void
    {
        $this->wardrobe = new SlidingWardrobeManualBuilder();
    }

    public function design(): void
    {
        $this->wardrobe->parts[] = "Design.";
    }

    public function drawing(): void
    {
        $this->wardrobe->parts[] = "Drawing.";
    }

    public function engineering(): void
    {
        $this->wardrobe->parts[] = "Engineering.";
    }

    public function  getProduct(): SlidingWardrobeManualBuilder
    {
        $result = $this->wardrobe;
        $this->reset();

        return $result;
    }
}


class SlidingWardrobeManualBuilder
{
    public $parts = [];

    public function listParts(): void
    {
        echo "Wardrobe include: " . implode(', ', $this->parts) . "\n\n";
    }
}


class Director
{
    private $builder;

    public function setBuilder(Builder $builder): void
    {
        $this->builder = $builder;
    }

    public function buildMinWardrobe(): void
    {
        $this->builder->design();
    }

    public function buildMaxWardrobe(): void
    {
        $this->builder->design();
        $this->builder->drawing();
        $this->builder->engineering();
    }
}

function clientCode(Director $director)
{
    $builder = new SlidingWardrobeBuilder();
    $director->setBuilder($builder);

    echo "First, \n";
    $director->buildMinWardrobe();
    $builder->getProduct()->listParts();

    echo "Then, \n";
    $builder->design();
    $builder->drawing();
    $builder->getProduct()->listParts();

    echo "In the end, \n";
    $director->buildMaxWardrobe();
    $builder->getProduct()->listParts();
}

$director = new Director();
clientCode($director);