<?php

namespace Patterns\Creational\AbstractFactory;

interface ClothingFactory
{
    public function createBoots(): Boots;

    public function createJacket(): Jacket;


}

class LeatherClothingFactory implements ClothingFactory
{
    public function createBoots(): Boots
    {
        return new LetherBoots();
    }

    public function createJacket(): Jacket
    {
        return new LetherJacket();
    }
}

class FurClothingFactory implements ClothingFactory
{
    public function createBoots(): Boots
    {
        return new FurBoots();
    }

    public function createJacket(): Jacket
    {
        return new FurJacket();
    }
}

interface Boots
{
    public function orderBoots(): string;
}

class LetherBoots implements Boots
{
    public function orderBoots(): string
    {
        return "LetherBoots: Geox";
    }
}

class FurBoots implements Boots
{
    public function orderBoots(): string
    {
        return "FurBoots: WANG";
    }
}

interface Jacket
{
    public function orderJacket(): string;
}

class LetherJacket implements Jacket
{
    public function orderJacket(): string
    {
        return "LetherJacket: GUESS";
    }
}

class FurJacket implements Jacket
{
    public function orderJacket(): string
    {
        return "FurJacket: Sinsay";
    }
}

class Customer
{
    public $boots;

    public $jacket;

    public function __construct($boots, $jacket)
    {
        $this->boots=$boots;
        $this->jacket=$jacket;
    }

    public function order(ClothingFactory $factory): string
    {
        $a = $factory->createBoots();
        $b = $factory->createJacket();

        if ($a !=null){
            return $this->boots;
        } elseif ($b !=null) {
            return $this->jacket;
        } else {
            echo "Error";
        }
    }
}

$customer = new Customer('boots', 'jacket');

echo "Customer making order of clothes: ";

echo $customer->order(new LeatherClothingFactory());




